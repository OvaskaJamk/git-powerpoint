# Git info


- [ ] Gitlab tunnukset luotu
- [ ] Git ladattu
- [ ] Sourcetree ladattu
- [ ] Oma repository luotu gitlabiin sekä omalle koneelle

## Tunnukset

https://gitlab.com/users/sign_up

| | |
| ------ | ------ |
| **Full name** | Julkinen, näkyy Gitlab profiilissa |
| **Username** | Julkinen, määrittää polun remotelle. gitlab.com/*username*/*repository* |
| **Email** | Ei välttämättä julkinen, commit historiassa näkyvä email voi olla eri kuin kirjautumiseen käytettävä |



![](img/signup.png)

## Ladattavat ohjelmat

### Perus git
https://git-scm.com/downloads

#### Asennus
Valitse alla olevat, voi valita kaikki jos haluaa.

![](img/gitinstall1.png)

Oletuseditorin valinta on makuasia, notepad++ tai visual studio code on varmasti hyvä.

![](img/gitinstall2.png)

Oletusasetuksella git komentoja voi ajaa myös muualta kuin Git Bash terminaalista.

![](img/gitinstall3.png)

En tiedä, mitä merkitystä tällä asetuksella on. Valitaan oletus.

![](img/gitinstall4.png)

En tiedä, mitä merkitystä tällä asetuksella on. Valitaan oletus.

![](img/gitinstall5.png)

En tiedä, mitä merkitystä tällä asetuksella on. Valitaan oletus.

![](img/gitinstall6.png)

Oletusasetuksella commit historian sotkeminen on hankalaa.

![](img/gitinstall7.png)

Valitaan oletukset. Git Credential Manageria tarvitaan myöhemmin.

![](img/gitinstall8.png)

Voi valita, jos kokee tarvitsevansa.

![](img/gitinstall9.png)

### Git GUI asennus

https://www.sourcetreeapp.com/

#### Asennus

Skip, ei rekisteröidä Bitbucket tunnuksia.

![](img/streeinstall1.png)

Ei asenneta Mercurialia, valitse Advanced Options alta "Configure automatic line ending handling by default"

![](img/streeinstall2.png)

Aseta nimi ja sähköposti, mitkä tulevat näkymään commit historiassa. Sourcetree asettaa ne myös globaaleiksi käyttäjäasetuksiksi.

![](img/streeinstall3.png)

Ei lisätä SSH avainta.

![](img/streeinstall4.png)

Sourcetree on nyt asennettu.

## Ensimmäisen oman repositoryn käyttöönottaminen

Aloitetaan luomalla Gitlabiin uusi projekti.

![](img/ekarepo1.png)

Anna projektille nimi. Nimi tulee näkymään myös projektin osoitteessa. Ääkkösten välttäminen on suotavaa, mutta niitä voi käyttää. Valitaan lisäasetus "Initialize repository with a README" niin pääsemme heti lataamaan repositoryn.

![](img/ekarepo2.png)

Kopioi repositoryn **HTTPS** osoite.

![](img/ekarepo3.png)

Liitä osoite Sourcetreen **Clone** valikossa. Sourcetree kysyy **Gitlab** tunnuksia. Myöhemmin tunnuksia ei tällä tietokoneella tarvitse enää laittaa. Repositoryn tallennuspaikan voi asettaa haluamaansa paikkaan.

![](img/ekarepo4.png)

Muokataan README tiedostoa esimerkiksi notepadilla. Tallenna tiedosto ja avaa Sourcetree. Jos FIle Status valikossa ei näy mitään paina F5. Unstaged files kohdassa tulisi näkyä, että README tiedostoa on muokattu. Klikkaamalla sitä oikealle avautuu näkymä, jossa nähdään muutokset. Tässä vaiheessa muutoksia ei vielä ole tallennettu.

![](img/ekarepo5.png)

Paina README vieressä olevaa plus-merkkiä. Nyt tiedosto on lisätty tallennettavaksi. Lisää vielä commitille viesti, ja paina sen jälkeen commit. Sen jälkeen muutos on tallennettu historiaan. (Jos commit ei toimi, kokeile mennä oikeassa kulmassa Settings->Advanced-> paina "Use global settings" pois->Ok ja yritä uudestaan)

![](img/ekarepo6.png)

Tässä vaiheessa muutokset on tallennettu vain omalle koneelle. Ladataan muutokset seuraavaksi Gitlabiin, eli remoteen. Paina **Push**.

![](img/ekarepo7.png)

Valitaan **Branch**, johon pushata. Meillä on vain master branch, joten valitaan se. Paina **Push**.

![](img/ekarepo8.png)

Avaamalla projekti Gitlabissa uudestaan README tiedostoon tehtyjen muutosten pitäisi näkyä.

![](img/ekarepo9.png)

Kokeillaan omalle koneelle muutosten lataamista. Valitse GItlabissa "New file" omassa repositoryssa.

![](img/ekarepo10.png)

Lisätään repositoryyn .gitignore tiedosto. .gitignore tiedosto määrittää, mitä tiedostoja kansion sisällä **ei** seurata. Esimerkiksi temppi tiedostoja tai omia asetuksia on turha repositoryyn ladata. Kuvitellaan, että lisäämme repositoryyn MS Office tiedostoja. Lisätään listalta MicrosoftOffice template. Muokkaa commit viestiä ja paina Commit Changes.

![](img/ekarepo11.png)

Ladataan muutokset Gitlabista omalle koneelle. Painetaan Sourcetreessa **Fetch**, jolloin commit historiaan ilmestyy remoten ja oman koneen repositoryjen ero. Painamalla **Pull** Sourcetree lataa muutokset. **Fetch** -komentoa ei ole pakko ajaa ennen **Pull** komentoa.

![](img/ekarepo12.png)

Valitaan branch, joka ladataan. Eli master.

![](img/ekarepo13.png)

.gitignore tiedoston tulisi nyt näkyä kansiossa.

Kokeillaan seuraavaksi ottaa jokin tietty tiedosto pois seurannasta. Luo jokin tiedosto kansioon (esim. turhatiedosto.txt). Sourcetreen File Status sivulla tulisi nyt näkyä, että kansiossa on nyt uusi tiedosto, jota ei vielä seurata (kysymysmerkki). Klikkaa tiedostoa oikealla ja valitse **Ignore...** .

![](img/ekarepo14.png)

Seuraavassa valikossa valitaan, mitä kaikkea halutaan olla seuraamatta. Esimerkiksi tietyn nimisen tiedoston voi ottaa pois, tai kaikki tiedostot tietyllä päätteellä. .gitignore säänntöistä lisää: https://www.atlassian.com/git/tutorials/saving-changes/gitignore

Lisätään vain tämä tietty tiedosto listalle. Tiedosto voisi sisältää esimerkiksi jotain yksityistä tietoa, mitä ei haluta julkisesti jaettavan.

![](img/ekarepo15.png)

.gitignore tiedostoon on tullut muutos. Tallennetaan se historiaan. Valitsemalla "Push changes immediately to origin/aster" muutokset menevät suoraan myös Gitlabiin.

![](img/ekarepo16.png)

